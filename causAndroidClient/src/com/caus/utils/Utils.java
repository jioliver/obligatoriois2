package com.caus.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import com.caus.activities.Main;
import com.caus.entities.User;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class Utils {
	
	private static AssetsPropertyReader assetsPropertyReader;
	
	private static User usuarioLogueado;
	
	private static String barcode;
	private static String officeID;
	private static String ip;
	private static String puerto;
	
	public static String intepretarQR(String barcode){
		char[] code_parse = null;
		
		String delims = "_";
		String[] tokens = barcode.split(delims);
		return tokens[0];

	}
	
//	public static User getEstudianteLogueado(){
//		
//		User user= new User();
//		user.setFirstName("Sebastian");
//		user.setUserid(01);
//		user.setLastName("Castellanos");
//		user.setEmail("caste86@gmail.com");
//		user.setDisponible(true);
//		return user;
//	}
	
//	public static int getFuncionarioLogueado(){
//		
//		User funcionario= new User();
//		funcionario.setFirstName("Santiago");
//		funcionario.setUserid(02);
//		funcionario.setLastName("Matalonga");
//		funcionario.setEmail("Santiago.matalonga");
//		funcionario.setDisponible(false);
//		return funcionario.getUserid();
//	}
//	
	
	public static String getIdOficinaByQR(String barcode){
		char[] code_parse = null;
		
		String delims = "_";
		String[] tokens = barcode.split(delims);
		String resp =tokens[1];
		return resp;

	}
	
	public static String getIp(){ 
	    return ip;
	}
	
	public static String getDefaultIp(){ 
		assetsPropertyReader = new AssetsPropertyReader(Main.getAppContext());
		Properties p = assetsPropertyReader.getProperties("config.properties");
		ip=p.getProperty("ip");
		return  p.getProperty("ip");
	}
	
	public static String getDefaultPort(){ 
		assetsPropertyReader = new AssetsPropertyReader(Main.getAppContext());
		Properties p = assetsPropertyReader.getProperties("config.properties");
		puerto = p.getProperty("port");
		return  p.getProperty("port");
	
	}
	public static String getPort(){ 
	    return puerto;
	}
	
	public static Bitmap decodeFile(File f,int WIDTH,int HIGHT){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH=WIDTH;
            final int REQUIRED_HIGHT=HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HIGHT)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        }
            catch (FileNotFoundException e) {
            	e.printStackTrace();
            }
        return null;
    }

	public static String getBarcode() {
		return barcode;
	}

	public static void setBarcode(String barcode) {
		Utils.barcode = barcode;
	}
	
	public static String getOfficeID() {
		return officeID;
	}

	public static void setOfficeID(String officeID) {
		Utils.officeID = officeID;
	}

	public static String getPuerto() {
		return puerto;
	}

	public static void setPuerto(String puerto) {
		Utils.puerto = puerto;
	}

	public static void setIp(String ip) {
		Utils.ip = ip;
	}
	
	
}
