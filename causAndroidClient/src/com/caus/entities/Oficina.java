package com.caus.entities;

public class Oficina {

	private int id;
	private String nombre;
	private String descripcion;
	
	
	@Override
	public String toString() {
		return "User [userid=" + id + ", firstName=" + nombre
				+ "]";
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
		
		
}
