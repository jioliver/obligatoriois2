package com.caus.services;

import java.util.ArrayList;
import java.util.List;

import com.caus.activities.AsyncConnection;
import com.caus.entities.Notificacion;
import com.caus.entities.Oficina;
import com.caus.entities.User;
import com.caus.utils.AssetsPropertyReader;
import com.caus.utils.Utils;
import com.fasterxml.jackson.databind.Module.SetupContext;
import com.sun.corba.se.pept.transport.Connection;

public class ServiceController {

	private static ServiceController  instance = null;
	
	private AssetsPropertyReader assetsPropertyReader;
	
	private static User user = new User();
    
	public static ServiceController getInstance(){
		if(instance==null){
			return new ServiceController();
		}else{
			return instance;
		}
	}
	
	public static User getLoguedUser(){
		return user;
	}
	
	public static  void setLoguedUser(User user){
		setUser(user);
	}
	
	
	
	@SuppressWarnings("unchecked")
	public User getUser(int user_id) {
		User user= new User();
		String codeParse=Utils.intepretarQR(user_id+"");
		try {
			new AsyncConnection().execute("http://localhost:8080/CausServer/service/greeting/image/2");
			AsyncConnection connection =  new AsyncConnection();
			
			String ip = Utils.getIp();
			String port = Utils.getPort();
			
			//String url = new String("http://192.168.1.104:8080/CausServer/service/greeting/allUsers/");
			String url = new String("http://"+ip+":"+port+"/CausServer/service/greeting/user/");
			
			user =  connection.retrieveUser(url,user_id+"");
			
			connection.cancel(true);
			
			return user;
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	
	
		
	}
	@SuppressWarnings("unchecked")
	public List<User> 	retrieveFuncListByOffice(String idOficina) {
		List<User> users= new ArrayList<User>();
		try {
			new AsyncConnection().execute("http://localhost:8080/CausServer/service/greeting/image/2");
			AsyncConnection connection =  new AsyncConnection();
			
			String ip = Utils.getIp();
			String port = Utils.getPort();
			
			//String url = new String("http://192.168.1.104:8080/CausServer/service/greeting/allUsers/");
			String url = new String("http://"+ip+":"+port+"/CausServer/service/greeting/getFuncsByOffice/");
			
			users =  connection.retrieveFuncListByOffice(url,idOficina);
			
			connection.cancel(true);
			
			return users;
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}
	
	@SuppressWarnings("unchecked")
	public Oficina 	retrieveOfficeById(String idOficina) {
		Oficina oficina= new Oficina();
		try {
			new AsyncConnection().execute("http://localhost:8080/CausServer/service/greeting/image/2");
			AsyncConnection connection =  new AsyncConnection();
			
			String ip = Utils.getIp();
			String port = Utils.getPort();
			
			//String url = new String("http://192.168.1.104:8080/CausServer/service/greeting/allUsers/");
			String url = new String("http://"+ip+":"+port+"/CausServer/service/greeting/getOffice/");
			
			oficina =  connection.retrieveOfficeById(url,idOficina);
			
			connection.cancel(true);
			
			return oficina;
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return oficina;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> getAllUsers(String codigoOficina) {
		List<User> users= new ArrayList<User>();
		try {
			new AsyncConnection().execute("http://localhost:8080/CausServer/service/greeting/image/2");
			AsyncConnection connection =  new AsyncConnection();
			
			String ip = Utils.getIp();
			String port = Utils.getPort();
			
			//String url = new String("http://192.168.1.104:8080/CausServer/service/greeting/allUsers/");
			String url = new String("http://"+ip+":"+port+"/CausServer/service/greeting/allUsers/");
			
			users =  connection.retrieveAllUsers(url,codigoOficina);
			
			connection.cancel(true);
			
			return users;
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Oficina> getOffices() {
		List<Oficina> oficinas= new ArrayList<Oficina>();
		try {
			new AsyncConnection().execute("http://localhost:8080/CausServer/service/greeting/image/2");
			AsyncConnection connection =  new AsyncConnection();
			
			//172.20.2.227			 192.168.1.104
			//String url = new String("http://192.168.1.104:8080/CausServer/service/greeting/allOfficines/");
			
			String ip = Utils.getIp();
			String port = Utils.getPort();
			//String url = new String("http://192.168.1.104:8080/CausServer/service/greeting/allUsers/");
			String url = new String("http://"+ip+":"+port+"/CausServer/service/greeting/allOfficines/");
			
			
			
			oficinas =  connection.retrieveAllOffices(url);
			
			connection.cancel(true);
			
			return oficinas;
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return oficinas;
	}

	
	
	@SuppressWarnings("unchecked")
	public User setAvailability(boolean disponibility,int userId,String message) {
		User user = null;
		try {
			new AsyncConnection().execute("http://localhost:8080/CausServer/service/greeting/image/2");
			AsyncConnection connection =  new AsyncConnection();
			
			String ip = Utils.getIp();
			String port = Utils.getPort();
			//String url = new String("http://192.168.1.104:8080/CausServer/service/greeting/allUsers/");
			String url = new String("http://"+ip+":"+port+"/CausServer/service/greeting/state");
			
			user =  connection.setAviability(url,disponibility,userId,message);
			
			connection.cancel(true);
			
			return user;
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}
	
	
	@SuppressWarnings("unchecked")
	public boolean setNotification(int funcId,int userId,int officeId,String userName,String message) {
		Boolean resp = null;
		try {
			new AsyncConnection().execute();
			AsyncConnection connection =  new AsyncConnection();
			
			String ip = Utils.getIp();
			String port = Utils.getPort();
			//String url = new String("http://192.168.1.104:8080/CausServer/service/greeting/allUsers/");
			String url = new String("http://"+ip+":"+port+"/CausServer/service/greeting/setNotification");
			
			resp =  connection.setNotification(url,funcId,userId,officeId,userName,message);
			
			connection.cancel(true);
		}catch (Exception e) {
			return false;
		}
		return resp;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Notificacion> getNotificationes(int funcId,int officeId) {
		ArrayList<Notificacion> resp = new ArrayList<Notificacion>();
		try {
			new AsyncConnection().execute();
			AsyncConnection connection =  new AsyncConnection();
			
			String ip = Utils.getIp();
			String port = Utils.getPort();
			//String url = new String("http://192.168.1.104:8080/CausServer/service/greeting/allUsers/");
			String url = new String("http://"+ip+":"+port+"/CausServer/service/greeting/getNotification/");
			
			resp =  (ArrayList<Notificacion>) connection.getNotification(url,funcId,officeId);
			
			connection.cancel(true);
		}catch (Exception e) {
			return resp;
		}
		return resp;
	}
	
	@SuppressWarnings("unchecked")
	public User getLoguedUser(String user,String pass) {
		User loguedUser = new User();
		try {
			new AsyncConnection().execute();
			AsyncConnection connection =  new AsyncConnection();
			
			String ip = Utils.getIp();
			String port = Utils.getPort();
			//String url = new String("http://192.168.1.104:8080/CausServer/service/greeting/allUsers/");
			String url = new String("http://"+ip+":"+port+"/CausServer/service/greeting/isLoguedUser/");
			
			loguedUser =  (User)connection.getLoguedUser(url,user,pass);
			
			connection.cancel(true);
		}catch (Exception e) {
			return null;
		}
		return loguedUser;
	}
	
	
	@SuppressWarnings("unchecked")
	public void retrieveMap(String id) {
	
		try {
			new AsyncConnection().execute("http://localhost:8080/CausServer/service/greeting/image/2");
			AsyncConnection connection =  new AsyncConnection();
			
			String ip = Utils.getIp();
			String port = Utils.getPort();
			String url = new String("http://"+ip+":"+port+"/CausServer/service/greeting/image/");
			//String url = new String("http://localhost:8080/CausServer/service/greeting/image/"+id);
	
			connection.retrieveImage(url,id);
			
			connection.cancel(true);
	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public static User getUser() {
		return user;
	}

	public static void setUser(User user) {
		ServiceController.user = user;
	}
	
}
