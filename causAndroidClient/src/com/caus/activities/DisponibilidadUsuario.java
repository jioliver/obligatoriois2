package com.caus.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.caus.entities.User;
import com.caus.services.ServiceController;
import com.caus.utils.Codigos;

@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
public class DisponibilidadUsuario extends Activity {
	String item;
	String id_func="";
	
	Button acceptButton;
	OnClickListener acceptListener;
	RadioButton rb_disponible=null;
	RadioButton rb_no_disponible=null;
	RadioButton rb_mensaje_personal=null;
	String message="Estuve por aqui";
	EditText editText;
	User usuarioLogueado;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.cambiar_disponibilidad);
        Intent intent = getIntent();
        id_func=intent.getStringExtra(Codigos.ID);
        
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroupPredef);
    	radioGroup.setVisibility(radioGroup.GONE);
    	editText = (EditText) findViewById(R.id.edit_personal);
    	editText.setVisibility(editText.GONE);
    	
    	rb_disponible = (RadioButton) findViewById(R.id.radio_disp);
    	rb_disponible.setChecked(Boolean.TRUE);
    	usuarioLogueado = ServiceController.getInstance().getLoguedUser();
       acceptListener = new OnClickListener() {
		
    	   @Override
			public void onClick(View v) {
    		   
    		   int user_id =usuarioLogueado.getUserid();
    		   rb_no_disponible = (RadioButton) findViewById(R.id.radio_no_disp);
    		   if(rb_no_disponible.isChecked()){
    			   rb_mensaje_personal = (RadioButton) findViewById(R.id.radio_personal);
    			   if(rb_mensaje_personal.isChecked()){
    				   if(editText.getText().toString().trim().equals("")){
    					   editText.setError("Por favor, complete la descripcion.");
    					   editText.setHint("Por favor, complete la descripcion.");
    				   }else{
    					   editText.setError(null);
    					   message=editText.getText().toString(); 
    				   }
    			   }
				   ServiceController.getInstance().setAvailability(((RadioButton) findViewById(R.id.radio_disp)).isChecked(), user_id,message);
				   Toast toast = Toast.makeText(getBaseContext(), "Operacion realizada con exito!", Toast.LENGTH_LONG);
				   toast.setGravity(Gravity.CENTER, 0, 0);
	               toast.show();
    		   }
    		   else{
				   ServiceController.getInstance().setAvailability(((RadioButton) findViewById(R.id.radio_disp)).isChecked(), user_id,message);
				   Toast toast = Toast.makeText(getBaseContext(), "Operacion realizada con exito!", Toast.LENGTH_LONG);
				   toast.setGravity(Gravity.CENTER, 0, 0);
	               toast.show();
    		   }
			}
       };
       
       
//       editText.setOnFocusChangeListener(new  OnFocusChangeListener() {
//		
//		@Override
//		public void onFocusChange(View v, boolean hasFocus) {
//			if(!editText.getText().toString().trim().equals("")){
//				   editText.setError(null);
//				   editText.setHint(null);
//			}
//			
//		}
//	});
       
       
       acceptButton = (Button)findViewById(R.id.aviabButton);
       
       acceptButton.setOnClickListener(acceptListener);
        
    }
	

	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	public void onRadioButtonClicked(View view) {
	    boolean checked = ((RadioButton) view).isChecked();
	    rb_disponible = ((RadioButton) view);
	    message =  rb_disponible.getText().toString();
	    RadioGroup radioGroup=null;
	    editText.setError(null);
	    // Check which radio button was clicked
	    switch(view.getId()) {
	        case R.id.radio_disp:
	            if (checked){
		            radioGroup = (RadioGroup) findViewById(R.id.radioGroupPredef);
	            	radioGroup.setVisibility(radioGroup.GONE);
	            	TextView tv =  (TextView)findViewById(R.id.predef_title);
	            	tv.setVisibility(view.GONE);
	            	editText = (EditText) findViewById(R.id.edit_personal);
	            	editText.setVisibility(editText.GONE);
	            	view.animate().setDuration(1000).alpha(100);
	            }
	            break;
	        case R.id.radio_no_disp:
	            if (checked){
	            	radioGroup = (RadioGroup) findViewById(R.id.radioGroupPredef);
	            	radioGroup.setVisibility(radioGroup.VISIBLE);
	            	view.animate().setDuration(1000).alpha(100);
	            }
	            break;
	      
	        case R.id.radio_no_disp_1:
	            if (checked){
	            	EditText editText = (EditText) findViewById(R.id.edit_personal);
	            	editText.setVisibility(editText.GONE);
	            }
	            break;
	        case R.id.radio_no_disp_2:
	            if (checked){
	            	editText = (EditText) findViewById(R.id.edit_personal);
	            	editText.setVisibility(editText.GONE);
	            	
	            	message =  rb_disponible.getText().toString();
	            }
	            break;
	        case R.id.radio_personal:
	            if (checked){
	            	EditText editText = (EditText) findViewById(R.id.edit_personal);
	            	editText.setVisibility(editText.VISIBLE);
	            }
	            break;
	    }
	}

}
