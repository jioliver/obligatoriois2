package com.caus.activities;

import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.caus.entities.User;
import com.caus.services.ServiceController;
import com.caus.utils.Codigos;
import com.caus.utils.Utils;

public class ListadoUsuarios extends Activity {
	String item;
	String code="";
	//String officeId="";
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.listado_usuarios);
        Intent intent = getIntent();
        code=intent.getStringExtra(Codigos.BAR_CODE);
        //officeId= intent.getStringExtra(Codigos.ID);
        String a  = Utils.getOfficeID();
        //Asumimos que esta clase se ejecuta  solo desde QRPasillo
        List<User> users = ServiceController.getInstance().getAllUsers(Codigos.CODIGO_PASILLO);

        cargarListadoPantalla(users);
       
    }
	
	
	public void cargarListadoPantalla(List<User> listadoUsers){
        
        final ListView listview = (ListView) findViewById(R.id.listPersons);

//        final ArrayList<String> list = new ArrayList<String>();
//        
//        for (User user : listadoUsers) {
//			list.add(String.valueOf(user.getUserid()));
//		}

        
        String[] web=new String[listadoUsers.size()];
        
        Integer[] imagesId = new Integer[listadoUsers.size()];
        
        //TODO: sustituir por  un hashmap , asi podemos levantar imagenes acordes a los nombres.
        for (int i = 0; i < listadoUsers.size(); i++) {
        	web[i]=String.valueOf(listadoUsers.get(i).getUserid());
        }
        
        int cont  = 0;
        int imageId=0;
        for (User user : listadoUsers) {
        	imageId= getResources().getIdentifier(user.getFirstName().toLowerCase()+"_"+user.getLastName().toLowerCase(),"drawable", getPackageName());
       		if(imageId==0){
       			if(user.getSexo().equals("M")){
       				imagesId[cont] =  R.drawable.avatar_male ;
       			}else{
       				imagesId[cont] =  R.drawable.avatar_female ;
       			}
       		}else{
       			imagesId[cont] = imageId;
       		}
       		cont ++;
		}
//        for (int i = 0; i < listadoUsers.size(); i++) {
//        	
//        	
//        	if(i%2==0)
//        		imagesId[i]=R.drawable.avatar_female;
//        	else
//        		imagesId[i]=R.drawable.avatar_male;
//        	
//        }
        
        
        
//        @SuppressWarnings("unchecked")
//		final ArrayAdapter<String> adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, list);
        
        CustomList adapter = new CustomList(this, web,imagesId,listadoUsers);
        
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @SuppressLint("NewApi")
			@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
			@Override
            public void onItemClick(AdapterView<?> parent, final View view,
                int position, long id) {
              item = (String) parent.getItemAtPosition(position);
              
              view.animate().setDuration(200).alpha(100).withEndAction(new Runnable() {
              //view.animate().x(50f).y(100f).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                    	Intent myIntent = new Intent(ListadoUsuarios.this, MenuFuncionario.class);
                    	myIntent.putExtra(Codigos.CODIGO_USUARIO, item);
                    	ListadoUsuarios.this.startActivity(myIntent);
                    	//!!!
                    	//officeId=null;
                    	//myIntent.putExtra(Codigos.ID, officeId);
//                    list.remove(item);
//                      adapter.notifyDataSetChanged();
//                      view.setAlpha(1);
                    }
                  });
            }

          });
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
