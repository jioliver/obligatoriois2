package com.caus.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.caus.services.ServiceController;
import com.caus.utils.Utils;

public class Configuracion extends Activity {

	EditText editTxtIp;
	EditText editTxtPuerto;
	Button aceptarButton;
	OnClickListener  actionListenerAceptar;
	@Override
    public void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
	        setContentView(R.layout.configuracion);
	        editTxtIp = (EditText)findViewById(R.id.editTextIp);
	        editTxtPuerto =(EditText)findViewById(R.id.editTextPuerto);
	        editTxtIp.setText(Utils.getDefaultIp());
	        editTxtPuerto.setText(Utils.getDefaultPort());
	        
	        actionListenerAceptar = new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					editTxtIp = (EditText)findViewById(R.id.editTextIp);
			        editTxtPuerto =(EditText)findViewById(R.id.editTextPuerto);
					Utils.setPuerto(editTxtPuerto.getText().toString());
					Utils.setIp(editTxtIp.getText().toString());
					Intent myIntent = new Intent(Configuracion.this, Main.class);
					Configuracion.this.startActivity(myIntent);
					
				}
			};
			aceptarButton = (Button)findViewById(R.id.submitConfiguration);
			aceptarButton.setOnClickListener(actionListenerAceptar); 
	}
}
