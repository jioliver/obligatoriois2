package com.caus.activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import com.caus.services.ServiceController;
import com.caus.utils.Codigos;

@SuppressLint("NewApi")
public class QRPasillo extends Activity {
	
	OnClickListener mapListener = null;
	OnClickListener funcListener = null;
	OnClickListener offListener = null;
	String id="";
	String barcode="";

	Button mapaButton;
	Button funcionarioButton;
	Button oficinasButton;
	String officeId="";
	ProgressDialog progressDialog;
	
	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.qrpasillo);
        Intent intent = getIntent();
        id = intent.getStringExtra(Codigos.CODIGO_PARAM);
        barcode =intent.getStringExtra(Codigos.BAR_CODE);
        ImageView imageView= (ImageView)findViewById(R.id.imageView1);
        imageView.setVisibility(View.INVISIBLE);

        mapListener = new OnClickListener() {
            public void onClick(View v) {
            	//Carga mapa y lo muestra en la vista actual
            	ServiceController.getInstance().retrieveMap(barcode);
            	runDialog(4);
            	
            	//progressDialog = ProgressDialog.show(QRPasillo.this, "Por favor espere...", "Renderizando mapa");
            	//cargarImagen();
            	//progressDialog.dismiss();
            }
        };
        funcListener = new OnClickListener() {
            public void onClick(View v) {
            	
            	Intent myIntent = new Intent(QRPasillo.this, ListadoUsuarios.class);
		    	myIntent.putExtra(Codigos.BAR_CODE, barcode);
		    	//officeId=Utils.getIdOficinaByQR(barcode);
		    	//Utils.setOfficeID(officeId);
		    	myIntent.putExtra("officeId", officeId);
	        	QRPasillo.this.startActivity(myIntent);
	        	 
            }
        };
        
        offListener  = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(QRPasillo.this, QROficina.class);
				myIntent.putExtra(Codigos.CODIGO_PARAM, id);
				myIntent.putExtra(Codigos.BAR_CODE, barcode);
				QRPasillo.this.startActivity(myIntent);
			}
		};
        
        mapaButton = (Button) findViewById(R.id.mapaButton);
        mapaButton.setOnClickListener(mapListener);
        
        funcionarioButton =(Button)findViewById(R.id.funcionarioButton);
        funcionarioButton.setOnClickListener(funcListener);
        
        oficinasButton=(Button)findViewById(R.id.officinasButton);
        oficinasButton.setOnClickListener(offListener);
        
        
    }
	
	
	private void runDialog(final int seconds)
	{
		progressDialog = ProgressDialog.show(this, "Por favor espere por favor...", "Renderizando mapa",true);

	    new Thread(new Runnable(){
	        public void run(){
	            try {
	            	Thread.sleep(seconds * 1000);
	                progressDialog.dismiss();
	                
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            }
	        }
	    }).start();
	    cargarImagen();
	}
	
	public void cargarImagen(){
		ImageView imageView= (ImageView)findViewById(R.id.imageView1);

        File sdCard = Environment.getExternalStorageDirectory();
		File dir = new File (sdCard.getAbsolutePath()+ "/imagenesOrt/"+barcode+".jpg");
		
		Bitmap bmpPosta = decodeFile(dir,600,600);
        		
		Drawable verticalImage = new BitmapDrawable(getResources(), bmpPosta);
        
		imageView.setVisibility(View.VISIBLE);
		
		imageView.setImageDrawable(verticalImage);
	}

	public Bitmap decodeFile(File f,int WIDTH,int HIGHT){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH=WIDTH;
            final int REQUIRED_HIGHT=HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HIGHT)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        }
            catch (FileNotFoundException e) {
            	e.printStackTrace();
            }
        return null;
    }
	
	
	
	@SuppressWarnings("unchecked")
	private void serviceImages(String id) {
	
		try {
			new AsyncConnection().execute("http://localhost:8080/SpringServiceSample/service/greeting/image/2");
			AsyncConnection connection =  new AsyncConnection();
			
			String url = new String("http://localhost:8080/SpringServiceSample/service/greeting/image/"+id);
	
			connection.retrieveImage(url,id);
			
			connection.cancel(true);
	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
