package com.caus.activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.caus.entities.User;
import com.caus.services.ServiceController;
import com.caus.utils.Codigos;

public class MenuFuncionario extends Activity {
	private String id="";
	OnClickListener disponibilidadListener = null;
	OnClickListener notificacionSubmitListener= null;
	OnClickListener notificacionListener = null;
	Button btnDisponibilidad;
	Button btnNotificacion;
	Button btnSubmmitNotificacion;
	User user;
	String id_oficina="";
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.menu_funcionario_oficina);
        
        Intent intent = getIntent();
        //Id del usuario Seleccionado en una listado anterior.
        id = intent.getStringExtra(Codigos.CODIGO_USUARIO);
        //id de la oficina del funcionario. (en caso que venga de leer un QR  oficina)
//id_oficina=  intent.getStringExtra(Codigos.ID);
        id_oficina=intent.getStringExtra(Codigos.ID);
        int identif = Integer.parseInt(id);
        user = ServiceController.getInstance().getUser(Integer.parseInt(id));
        cargarTextos(user);
        
        disponibilidadListener = new OnClickListener() {
            public void onClick(View v) {
            	//mover  a  pantalla disponiblidad  nueva.
            	
            	setContentView(R.layout.person);
            	
            	cargarDatosDisponibilidad();
            	
            }
        };
        
        notificacionListener= new OnClickListener() {
            public void onClick(View v) {
            	//mover  a  pantalla disponiblidad  nueva.
            	setContentView(R.layout.notificacion_visita);
            	btnSubmmitNotificacion=(Button) findViewById(R.id.btnSubmmitNotificacion);
                btnSubmmitNotificacion.setOnClickListener(notificacionSubmitListener);
            	
            }
        };
        
        notificacionSubmitListener= new OnClickListener() {
            public void onClick(View v) {
            	//mover  a  pantalla disponiblidad  nueva.
            	EditText txtMessage = (EditText) findViewById(R.id.edit_msjNotificacion);
            	//TODO: Agregar los parametos reales.Obtener  la oficina y el func de la pantalla anterior.
            	//TODO: Metodo generico que  nos retorne el usuario logueado 
            	User  loguedUser = ServiceController.getInstance().getLoguedUser();
            	
            	ServiceController.getInstance().setNotification(Integer.parseInt(id), loguedUser.getUserid(),Integer.parseInt(id_oficina)/*002*/, loguedUser.getFirstName(), txtMessage.getText().toString());
            	Toast toast = Toast.makeText(getBaseContext(), "Se ha notificado con exito!", Toast.LENGTH_LONG);
            	toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        };
        
        
        
        
        btnDisponibilidad=(Button) findViewById(R.id.btnDisponibilidad);
        btnDisponibilidad.setOnClickListener(disponibilidadListener);
        
        btnNotificacion=(Button) findViewById(R.id.btnNotificacion);
        
        if(id_oficina==null){
        	btnNotificacion.setVisibility(View.GONE);
        }else{
        	btnNotificacion.setOnClickListener(notificacionListener);
        }
        
        //aviablButton=(Button) findViewById(R.id.availability);
      //  obtenerImagen(id);
//cargarImagen();
        //cargarListadoPantalla(users);
       
    }
	
	@SuppressWarnings("unchecked")
	private void obtenerImagen(String id) {
	
		try {
			new AsyncConnection().execute("http://localhost:8080/SpringServiceSample/service/greeting/image/2");
			AsyncConnection connection =  new AsyncConnection();
			
			String url = new String("http://localhost:8080/SpringServiceSample/service/greeting/image/"+id);
	
			connection.retrieveImage(url,id);
			
			connection.cancel(true);
	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public void cargarTextos(User user){
		TextView txtName=(TextView)findViewById(R.id.textViewName);
		TextView txtLastName=(TextView)findViewById(R.id.textViewLastName);
		txtName.setText(user.getFirstName());
		txtLastName.setText(user.getLastName());
		ImageView contactImage = (ImageView) findViewById(R.id.imageViewContact);
		int imageId= getResources().getIdentifier(user.getFirstName().toLowerCase()+"_"+user.getLastName().toLowerCase(),"drawable", getPackageName());
		if(imageId==0){
   			if(user.getSexo().equals("M")){
   				contactImage.setImageResource(R.drawable.avatar_male) ;
   			}else{
   				contactImage.setImageResource(R.drawable.avatar_female) ;
   			}
		}else{
			contactImage.setImageResource(imageId);
		}
	}
	public void cargarDatosDisponibilidad(){
		TextView txtDisponibilidad=(TextView)findViewById(R.id.textViewDisponibilidad);
		EditText editText = (EditText) findViewById(R.id.editTextMensajeNoDispo);
		TextView txtMensaje = (TextView) findViewById(R.id.textViewMensaje);
		editText.setInputType(InputType.TYPE_NULL);
		
		int imageId= getResources().getIdentifier(user.getFirstName().toLowerCase()+"_"+user.getLastName().toLowerCase(),"drawable", getPackageName());
		ImageView contactImage = (ImageView) findViewById(R.id.imageViewContactDisponibilidad);
    	
   		if(imageId==0){
   			if(user.getSexo().equals("M")){
   				contactImage.setImageResource(R.drawable.avatar_male);
   			}else{
   				contactImage.setImageResource(R.drawable.avatar_female);
   			}
   		}else{
   			contactImage.setImageResource(imageId);
   		}
		
		editText.setText(user.getMensajeNoDisponible());

		if(user.isDisponible()){
			txtDisponibilidad.setText("Disponible");
			editText.setVisibility(View.INVISIBLE);
			txtMensaje.setVisibility(View.INVISIBLE);
		}else{
			txtDisponibilidad.setText("No Disponible");
		}
	}
	
	
	public void cargarImagen(){
		ImageView imageView= (ImageView)findViewById(R.id.personalImage);

        File sdCard = Environment.getExternalStorageDirectory();
		File dir = new File (sdCard.getAbsolutePath()+ "/imagenesOrt/"+getId()+".jpg");
		
		Bitmap bmpPosta = decodeFile(dir,600,600);
        		
		Drawable verticalImage = new BitmapDrawable(getResources(), bmpPosta);
        
		imageView.setVisibility(View.VISIBLE);
		
		imageView.setImageDrawable(verticalImage);
	}
	
	
	public Drawable loadContactImage(String  imageId){
        File sdCard = Environment.getExternalStorageDirectory();
		File dir = new File (sdCard.getAbsolutePath()+ "/imagenesOrt/"+imageId+".jpg");
		
		Bitmap bmpPosta = decodeFile(dir,400,400);
        		
		Drawable verticalImage = new BitmapDrawable(getResources(), bmpPosta);
        
		return verticalImage;
	}
	
	public Bitmap decodeFile(File f,int WIDTH,int HIGHT){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH=WIDTH;
            final int REQUIRED_HIGHT=HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HIGHT)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        }
            catch (FileNotFoundException e) {
            	e.printStackTrace();
            }
        return null;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

}
