package com.caus.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.caus.entities.User;

public class Estudiante extends Activity {
	private String id="";
	OnClickListener disponibilidadListener = null;
	Button btnDisponibilidad;
	User user;
	String id_oficina;
	
	String user_id;
    String user_name;
    String fecha;
    String mensaje;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.estudiante);
        
        Intent intent = getIntent();
        //Id del usuario Seleccionado en una listado anterior.
        String[] notificacion = intent.getStringArrayExtra("notificacion");
        
         user_id=notificacion[0];
         user_name=notificacion[1];
         fecha=notificacion[2];
         mensaje=notificacion[3];
         cargarTextos();
        //id de la oficina del funcionario. (en caso que venga de leer un QR  oficina)
        
    }
	

	public void cargarTextos(){
		TextView txtUserName=(TextView)findViewById(R.id.editTextUserName);
		TextView txtFecha=(TextView)findViewById(R.id.editTextFecha);
		TextView txtMensaje=(TextView)findViewById(R.id.editTextMensaje);
		
		txtUserName.setText(user_name);
		txtFecha.setText(fecha);
		txtMensaje.setText(mensaje);
	}
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

}
