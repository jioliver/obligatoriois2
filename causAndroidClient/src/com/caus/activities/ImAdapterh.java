package com.caus.activities;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;



public class ImAdapterh extends BaseAdapter{

    File dir=new File(Environment.getExternalStorageDirectory(),"/imagenesOrt");
    
		int count=dir.list().length; 
		
		String[] fileNames = dir.list();
		
		private Context mContext;
		
		 public ImAdapterh(Context c) {
		     mContext = c;
		 }
		
		 public int getCount() {
		     return count;
		 }
		
		 public Object getItem(int position) {
		     return null;
		 }
		
		 public long getItemId(int position) {
		     return 0;
		 }

 // create a new ImageView for each item referenced by the Adapter
 public View getView(int position, View convertView, ViewGroup parent) {
     ImageView imageView = null;        


//   for(String bitmapFileName : fileNames)
//   {
	   
	   if(fileNames[0].toString().equals("anonymous.jpg")){
		   if (convertView == null) 
	        {  // if it's not recycled, initialize some attributes
	            imageView = new ImageView(mContext);
	            imageView.setLayoutParams(new GridView.LayoutParams(200, 200));
	            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
	            imageView.setPadding(8, 8, 8, 8);                   

	            Bitmap bmp = BitmapFactory.decodeFile(dir.getPath() + "/" + fileNames[0].toString());
	            System.out.println(dir);
	            imageView.setImageBitmap(bmp);  
	         }else 
	         {
	         imageView = (ImageView) convertView;            

	         }
	  // }
        
 } 
     return imageView;
     }
}
