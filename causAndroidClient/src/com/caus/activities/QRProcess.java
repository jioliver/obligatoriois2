package com.caus.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.caus.entities.User;
import com.caus.integration.android.IntentIntegrator;
import com.caus.integration.android.IntentResult;
import com.caus.services.ServiceController;
import com.caus.utils.Codigos;
import com.caus.utils.Utils;

public class QRProcess extends Activity implements SensorEventListener,OnClickListener{

	private boolean mInitialized;
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private String barcode="";
	private static Context context;
	
	OnClickListener listener1 = null;
	OnClickListener listener2 = null;
	Button login;
	Button qr_button;
	ImageView  imageView;

	
	/** Called when the activity is first created. */
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context= getApplicationContext();
		
		setContentView(R.layout.main);
		
		mInitialized = false;
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		//mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		mSensorManager.registerListener(this, mAccelerometer,
				SensorManager.SENSOR_DELAY_NORMAL);
		
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
	     .detectAll()
	     .penaltyLog()
	     .build();
		StrictMode.setThreadPolicy(policy);
	
		 setContentView(R.layout.main);
		 
        listener2 = new OnClickListener() {
            public void onClick(View v) {
            	IntentIntegrator integrator = new IntentIntegrator(QRProcess.this);
            	integrator.initiateScan();
            }
        };
        
		 qr_button = (Button) findViewById(R.id.button2);
		 qr_button.setOnClickListener(listener2);
	}
	
	
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		  IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		  if (scanResult != null) {
		    getBarcode();
		    String type;
		    setBarcode(scanResult.getContents());
		    type=scanResult.getFormatName();
		    Utils.setBarcode(barcode);
		    String qrNav = Utils.intepretarQR(barcode);
		    navegar(qrNav);
		    
		  }
	            	            
		}
	
	public void navegar(String qrNav){
		if(qrNav.equals(Codigos.CODIGO_PASILLO)){
			Intent myIntent = new Intent(QRProcess.this, QRPasillo.class);
	    	myIntent.putExtra(Codigos.CODIGO_PARAM, Codigos.CODIGO_PASILLO);
	    	myIntent.putExtra(Codigos.BAR_CODE, getBarcode());
        	QRProcess.this.startActivity(myIntent);
	    }else
	    {
	    	if(qrNav.equals(Codigos.CODIGO_OFICINA_INTERNO)){
	    		User user = ServiceController.getInstance().getLoguedUser();
	    		if(user.getTipo().equals("F")){
	    			Intent myIntent = new Intent(QRProcess.this, QROficina.class);
	    			myIntent.putExtra(Codigos.CODIGO_PARAM, Codigos.CODIGO_OFICINA_INTERNO);
	    			myIntent.putExtra(Codigos.BAR_CODE, getBarcode());
	    			myIntent.putExtra(Codigos.ID, user.getUserid()+"");
	    			QRProcess.this.startActivity(myIntent);
	    		}else{
	    			Toast.makeText(getApplicationContext(), "Este usuario no tiene privilegios para acceder a este codigo", Toast.LENGTH_LONG).show();
	    		}
		    }
	    else{
    		if(qrNav.equals(Codigos.CODIGO_OFICINA_EXTERNO)){
			    	Intent myIntent = new Intent(QRProcess.this, QROficina.class);
			    	String id_oficina = Utils.getIdOficinaByQR(getBarcode());
			    	Utils.setOfficeID(id_oficina);
			    	myIntent.putExtra(Codigos.ID, id_oficina);
			    	myIntent.putExtra(Codigos.CODIGO_PARAM, Codigos.CODIGO_OFICINA_EXTERNO);
			    	myIntent.putExtra(Codigos.BAR_CODE, getBarcode());
		        	QRProcess.this.startActivity(myIntent);
		    }
	    }
	    	
	    }
		
	}
	
	
	
	protected void onResume() {
		super.onResume();
		mSensorManager.registerListener(this, mAccelerometer,
				SensorManager.SENSOR_DELAY_NORMAL);
	}

	protected void onPause() {
		super.onPause();
		mSensorManager.unregisterListener(this);
		
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// can be safely ignored for this demo
	}

//	@Override
//	public void onClick(View v) {
//		Toast.makeText(getApplicationContext(), "Ando!", Toast.LENGTH_LONG).show();
//		
//		IntentIntegrator integrator = new IntentIntegrator(this);
//    	integrator.initiateScan();
//		
//	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
//		if(getBarcode().equals("")){
//			savedInstanceState.putString("imageID",savedInstanceState.getString("imageID"));
//		}  else{
//			savedInstanceState.putString("imageID",getBarcode());
//		}
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
	  super.onRestoreInstanceState(savedInstanceState);
	    // Restore UI state from the savedInstanceState.
	    // This bundle has also been passed to onCreate.
		//	  String myString = savedInstanceState.getString("imageID");
		//	  if(!myString.equals("")){
		//	  
		//		  ImageView imageView= (ImageView)findViewById(R.id.imageView1);
		//	
		//	      File sdCard = Environment.getExternalStorageDirectory();
		//			File dir = new File (sdCard.getAbsolutePath()+ "/imagenesOrt/"+myString+".jpg");
		//			
		//			Bitmap bmpPosta = decodeFile(dir,400,300);
		//	      		
		//			Drawable verticalImage = new BitmapDrawable(getResources(), bmpPosta);
		//	      
		//			imageView.setImageDrawable(verticalImage);
		//	  }

	}


	public String getBarcode() {
		return barcode;
	}


	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	
	public static Context getAppContext() {
        return QRProcess.context;
    }



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
}