package com.caus.activities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.caus.entities.Notificacion;
import com.caus.entities.User;
import com.caus.services.ServiceController;
import com.caus.utils.Codigos;

public class ListadoNotificaciones extends Activity {
	String item;
	String code="";
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.listado_notificaciones);
        Intent intent = getIntent();
        //Obtener  usuario logueado.
        User usuarioLogueado = ServiceController.getInstance().getLoguedUser();
        
        code=intent.getStringExtra(Codigos.BAR_CODE);
        
		ArrayList<Notificacion> notificaciones = ServiceController.getInstance().getNotificationes(usuarioLogueado.getUserid(), 1);
		cargarListadoNotificacionesPantalla(notificaciones);
        
       
    }
	
	
public void cargarListadoNotificacionesPantalla(List<Notificacion> listadoNotificaciones){
        
        final ListView listview = (ListView) findViewById(R.id.listNotifications);

        String[] web=new String[listadoNotificaciones.size()];
        
        for (int i = 0; i < listadoNotificaciones.size(); i++) {
        	
        	Calendar cal = Calendar.getInstance();
    		cal.add(Calendar.DATE, 1);
    		SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    		cal.setTime(listadoNotificaciones.get(i).getFecha());
    		String formatted = format1.format(cal.getTime());	
        	
        	web[i]=listadoNotificaciones.get(i).getUserId()+"#"+listadoNotificaciones.get(i).getUserName()+"#"+formatted+"#"+listadoNotificaciones.get(i).getMessage();
        }
        
        Integer[] imageId = new Integer[listadoNotificaciones.size()];
        for (int i = 0; i < listadoNotificaciones.size(); i++) {
        	
        	
        	if(i%2==0){
        		imageId[i] = R.drawable.avatar_female;
        	}else{
        		imageId[i] = R.drawable.avatar_male;
        	}
        }
        
        CustomNotificationsList adapter = new CustomNotificationsList(this, web,imageId,listadoNotificaciones);
        
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @SuppressLint("NewApi")
			@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
			@Override
            public void onItemClick(AdapterView<?> parent, final View view,
                int position, long id) {
              final String item = (String) parent.getItemAtPosition(position);
             // final User item2 = (User) parent.getItemAtPosition(position);
              view.animate().setDuration(500).alpha(100).withEndAction(new Runnable() {
              //view.animate().x(50f).y(100f).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                      
                    	TextView txtMensaje = (TextView)findViewById(R.id.txt3);
                    	txtMensaje.setVisibility(View.VISIBLE);
                    	Intent myIntent = new Intent(ListadoNotificaciones.this, Estudiante.class);
                    	String[] notificacion = item.split("#");
                    	
                    	myIntent.putExtra("notificacion",notificacion);
                    	ListadoNotificaciones.this.startActivity(myIntent);
//                      list.remove(item);
//                      adapter.notifyDataSetChanged();
//                      view.setAlpha(1);
                    }
                  });
            }

          });
		
	}

	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
