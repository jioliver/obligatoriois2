package com.caus.activities;

import java.util.ArrayList;
import java.util.List;

import com.caus.entities.User;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomList extends ArrayAdapter<String> {
	private final Activity context;
	private final String[] web;
	private final Integer[] imageId;
	private final List<User> listadoUsers=new ArrayList<User>();

	public CustomList(Activity context, String[] web, Integer[] imageId) {
		super(context, R.layout.listado_layout, web);
		this.context = context;
		this.web = web;
		this.imageId = imageId;
	}
	public CustomList(Activity context, String[] web, Integer[] imageId,List<User> listadoUsers) {
		super(context, R.layout.listado_layout, web);
		this.context = context;
		this.web = web;
		this.imageId = imageId;
		this.listadoUsers.addAll(listadoUsers);
	}
	

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.listado_layout, null, true);
		TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
		
		txtTitle.setText(listadoUsers.get(position).getFirstName());
		imageView.setImageResource(imageId[position]);
		return rowView;
	}
}
