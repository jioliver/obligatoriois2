package com.caus.activities;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.caus.entities.Oficina;
import com.caus.entities.User;
import com.caus.services.ServiceController;
import com.caus.utils.Codigos;
import com.caus.utils.Utils;

public class QROficina extends Activity {
	String barcode;
	String code;
	String officeId;
	String id_func;
	String  codigoQrOficina;
	
	OnClickListener aviabListener = null;
	OnClickListener historyListener = null;
	Button aviablButton;
	Button historyButton;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        
        aviabListener = new OnClickListener() {
            public void onClick(View v) {
            	
            	Intent myIntent = new Intent(QROficina.this, DisponibilidadUsuario.class);
		    	myIntent.putExtra(Codigos.ID, id_func);
	        	QROficina.this.startActivity(myIntent);
            }
        };
        
        historyListener= new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				//T0DO: obtener el usuario logueado y los datos de la oficina del barcode. y  pasarlos
				Intent myIntent = new Intent(QROficina.this, ListadoNotificaciones.class);
		    	myIntent.putExtra(Codigos.BAR_CODE,barcode);
		    	myIntent.putExtra(Codigos.CODIGO_OFICINA_INTERNO,officeId);
	        	QROficina.this.startActivity(myIntent);
			}
		};
        
        Intent intent = getIntent();
        barcode=intent.getStringExtra(Codigos.BAR_CODE);
        code=intent.getStringExtra(Codigos.CODIGO_PARAM);
        officeId=intent.getStringExtra(Codigos.ID);
        codigoQrOficina=Utils.intepretarQR(barcode);
        
        if(code.equals(Codigos.CODIGO_OFICINA_EXTERNO)){
        	setContentView(R.layout.oficina_externo);
        	Oficina oficina = ServiceController.getInstance().retrieveOfficeById(officeId);
        	displayInfoOficina(oficina);
        	List<User> users = ServiceController.getInstance().retrieveFuncListByOffice(officeId);
        	cargarListadoUsuariosPantalla(users);
        }
        
        else{
        	if(code.equals(Codigos.CODIGO_OFICINA_INTERNO)){
        		setContentView(R.layout.oficina_interno);
        		id_func=intent.getStringExtra(Codigos.ID);
        		
        		aviablButton=(Button) findViewById(R.id.availability);
        		aviablButton.setOnClickListener(aviabListener);
        		
        		historyButton =(Button)findViewById(R.id.history_button);
        		historyButton.setOnClickListener(historyListener);
        		
        		
        		//User  user = ServiceController.getInstance().setDisponibility(barcode, true, 1);
        		
                //En este caso viene de Main, cuando lee codio oficina Interno.
        	}
        	else{
                
            	if(code.equals(Codigos.CODIGO_PASILLO)){
            		setContentView(R.layout.oficina_externo_pasillo);
            		//En este caso viene de Main, cuando lee codio oficina Interno.
            		List<Oficina> users = ServiceController.getInstance().getOffices();
                	displayOffices(users);
            	}
            }
        }
    }
	public  void displayInfoOficina(Oficina oficina){
		TextView txtOficina = (TextView) findViewById(R.id.idOficina);
		txtOficina.setText(oficina.getNombre());
		
	}
	
	public void cargarListadoUsuariosPantalla(List<User> listadoUsers){
        
        final ListView listview = (ListView) findViewById(R.id.listFunc);

        //final ArrayList<User> list = new ArrayList<User>();
        
        String[] web=new String[listadoUsers.size()];
        
        for (int i = 0; i < listadoUsers.size(); i++) {
        	web[i]=listadoUsers.get(i).getUserid()+"";
        }
        
        Integer[] imageId = new Integer[listadoUsers.size()];
        for (int i = 0; i < listadoUsers.size(); i++) {
        	if(i%2==0){
        		imageId[i] = R.drawable.avatar_female;
        	}else{
        		imageId[i] = R.drawable.avatar_male;
        	}
        }
        
        @SuppressWarnings("unchecked")
        
        CustomList adapter = new CustomList(this, web,imageId,listadoUsers);
        
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @SuppressLint("NewApi")
			@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
			@Override
            public void onItemClick(AdapterView<?> parent, final View view,
                int position, long id) {
              final String item = (String) parent.getItemAtPosition(position);
              view.animate().setDuration(500).alpha(100).withEndAction(new Runnable() {
              //view.animate().x(50f).y(100f).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                      
                    	Intent myIntent = new Intent(QROficina.this, MenuFuncionario.class);
                    	myIntent.putExtra(Codigos.CODIGO_PARAM, code);
                    	myIntent.putExtra(Codigos.CODIGO_OFICINA_EXTERNO, codigoQrOficina);
                    	myIntent.putExtra(Codigos.ID,officeId);
                    	//Aca se deberia setear  el id real del   usuario, pero para  eso  hay q  carbar  objetos usuario en la lista
                    	//codigo_usuario es el user ID
                    	int codigo_usuario=Integer.parseInt(item);
                    	myIntent.putExtra(Codigos.CODIGO_USUARIO,item);
                    	myIntent.putExtra(Codigos.CODIGO_PARAM, code);
                    	QROficina.this.startActivity(myIntent);
                    	
//                    list.remove(item);
//                      adapter.notifyDataSetChanged();
//                      view.setAlpha(1);
                    }
                  });
            }

          });
		
	}
	
	
	
	//Deberia rederigir a la informacion de las oficinas  y no a la  persona como  lo esta haciendo en  este  momento.
	public void displayOffices(List<Oficina> listadoOficinas){
        
        final ListView listview = (ListView) findViewById(R.id.listFuncPasillo);

        final ArrayList<String> list = new ArrayList<String>();
        
        for (Oficina user : listadoOficinas) {
			list.add(user.getNombre());
		}
        
        String[] web=new String[listadoOficinas.size()];
        
        for (int i = 0; i < listadoOficinas.size(); i++) {
        	//web[i]=String.valueOf(listadoOficinas.get(i).getNombre());
        	web[i]=listadoOficinas.get(i).getId()+"#"+listadoOficinas.get(i).getNombre()+"#"+listadoOficinas.get(i).getDescripcion();
        }
        
        
        Integer[] imageId = new Integer[listadoOficinas.size()];
        
        for (int i = 0; i < listadoOficinas.size(); i++) {
        	imageId[i]=R.drawable.office_icon;
        }
        

        CustomOfficesList adapter = new CustomOfficesList(this, web,imageId,listadoOficinas);
        
        listview.setAdapter(adapter);


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @SuppressLint("NewApi")
			@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
			@Override
            public void onItemClick(AdapterView<?> parent, final View view,
                int position, long id) {
              final String item = (String) parent.getItemAtPosition(position);
              view.animate().setDuration(500).alpha(100).withEndAction(new Runnable() {
              //view.animate().x(50f).y(100f).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                      
                    	
                    	Intent myIntent = new Intent(QROficina.this, OficinaInfo.class);
                    	String[] oficina = item.split("#");
                    	
                    	myIntent.putExtra("oficina",oficina);
                    	QROficina.this.startActivity(myIntent);
                    	
                    }
                  });
            }

          });
	}
	
	
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String code) {
		this.barcode = code;
	}

}
