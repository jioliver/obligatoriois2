package com.caus.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.caus.entities.Oficina;

public class CustomOfficesList extends ArrayAdapter<String> {
	private final Activity context;
	private final String[] web;
	private final Integer[] imageId;
	private final List<Oficina> listadoUsers=new ArrayList<Oficina>();

	public CustomOfficesList(Activity context, String[] web, Integer[] imageId) {
		super(context, R.layout.listado_layout, web);
		this.context = context;
		this.web = web;
		this.imageId = imageId;
	}
	public CustomOfficesList(Activity context, String[] web, Integer[] imageId,List<Oficina> listadoUsers) {
		super(context, R.layout.listado_layout, web);
		this.context = context;
		this.web = web;
		this.imageId = imageId;
		this.listadoUsers.addAll(listadoUsers);
	}
	

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.listado_layout, null, true);
		TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
		TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
		TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
		txt2.setVisibility(View.INVISIBLE);
		txt3.setVisibility(View.INVISIBLE);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
		
		txtTitle.setText(listadoUsers.get(position).getNombre());
		imageView.setImageResource(imageId[position]);
		return rowView;
	}
}
