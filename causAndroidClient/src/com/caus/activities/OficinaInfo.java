package com.caus.activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import com.caus.services.ServiceController;
import com.caus.utils.Utils;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class OficinaInfo extends Activity {

	
	String officeName;
    String officeDescription;
    String officeId;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.oficina);
        
        Intent intent = getIntent();
        //Officina seleccionada en la lista de oficinas de QR publico.
        String[] office = intent.getStringArrayExtra("oficina");
        
        officeId=office[0];
        officeName=office[1];
        officeDescription=office[2];
        displayInfo();
        ServiceController.getInstance().retrieveMap(Utils.getBarcode()+"#"+officeId);
        cargarImagen();
    }
	

	public void displayInfo(){
		TextView txtOfficeName=(TextView)findViewById(R.id.textViewOfficeName);
		EditText txtOfficeDescription=(EditText)findViewById(R.id.infoOffice);
		txtOfficeDescription.setInputType(InputType.TYPE_NULL);
		
		txtOfficeName.setText(officeName);
		txtOfficeDescription.setText(officeDescription);
	}
	
	
	public void cargarImagen(){
		ImageView imageView= (ImageView)findViewById(R.id.imageView1);

        File sdCard = Environment.getExternalStorageDirectory();
		File dir = new File (sdCard.getAbsolutePath()+ "/imagenesOrt/"+ Utils.getBarcode()+"#"+officeId+".jpg");
		
		Bitmap bmpPosta = Utils.decodeFile(dir,600,600);
        		
		Drawable verticalImage = new BitmapDrawable(getResources(), bmpPosta);
        
		imageView.setVisibility(View.VISIBLE);
		
		imageView.setImageDrawable(verticalImage);
	}

	

}
