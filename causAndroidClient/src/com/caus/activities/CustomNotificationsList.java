package com.caus.activities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.caus.entities.Notificacion;

public class CustomNotificationsList extends ArrayAdapter<String> {
	private final Activity context;
	private final String[] web;
	private final Integer[] imageId;
	private final List<Notificacion > listadoNotificaciones=new ArrayList<Notificacion>();

	public CustomNotificationsList(Activity context, String[] web, Integer[] imageId) {
		super(context, R.layout.listado_layout, web);
		this.context = context;
		this.web = web;
		this.imageId = imageId;
	}
	public CustomNotificationsList(Activity context, String[] web, Integer[] imageId,List<Notificacion> listadoNotificaciones) {
		super(context, R.layout.listado_layout, web);
		this.context = context;
		this.web = web;
		this.imageId = imageId;
		this.listadoNotificaciones.addAll(listadoNotificaciones);
	}
	

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.listado_layout, null, true);
		TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
		TextView txtMensaje = (TextView) rowView.findViewById(R.id.txt2);
		TextView txtFecha = (TextView) rowView.findViewById(R.id.txt3);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
		
		txtTitle.setText(listadoNotificaciones.get(position).getUserName());
		txtTitle.setPadding(30, 40, 10, 10);
		txtTitle.setTypeface(txtTitle.getTypeface(), Typeface.BOLD);
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 1);
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");

		cal.setTime(listadoNotificaciones.get(position).getFecha());
		String formatted = format1.format(cal.getTime());	
		
		txtFecha.setText("Hora: "+ formatted);
		txtFecha.setPadding(40, 40, 0, 0);
		txtFecha.setTypeface(txtFecha.getTypeface(), Typeface.BOLD);
		
		txtMensaje.setText("Mensaje: "+listadoNotificaciones.get(position).getMessage());
		
		txtMensaje.setPadding(txtFecha.getPaddingLeft() + 40, 40, 0, 0);
		txtMensaje.setTypeface(txtFecha.getTypeface(), Typeface.BOLD);
		
		txtMensaje.setVisibility(View.VISIBLE);
		imageView.setImageResource(imageId[position]);
		return rowView;
	}
}
