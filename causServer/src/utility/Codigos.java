package utility;

public class Codigos {
	
	public static final String CODIGO_OFICINA_INTERNO="OI";
	public static final String CODIGO_OFICINA_EXTERNO="OE";
	public static final String CODIGO_PASILLO="P";
	public static final String USUARIO_FUNCIONARIO="F";
	public static final String USUARIO_ESTUDIANTE="E";
	public static final String DOMINIO_FUNCIONARIO="ort.edu.uy";
	
}
