package com.programmingfree.springservice.domain;

import java.util.ArrayList;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

public class User {
	private int userid;
	private String firstName;
	private String lastName;	
	private String email;
	private String password;
	private boolean disponible;
	private String mensajeNoDisponible;
	private String tipo;
	private String sexo;
	
	
	private  ArrayList<Notificacion> notificaciones = new  ArrayList<Notificacion>();
	
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "User [userid=" + userid + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email="
				+ email + "]";
	}
	public boolean isDisponible() {
		return disponible;
	}
	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}
	public ArrayList<Notificacion> getNotificaiones() {
		return notificaciones;
	}
	public void setNotificaiones(ArrayList<Notificacion> notificaiones) {
		this.notificaciones = notificaiones;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getMensajeNoDisponible() {
		return mensajeNoDisponible;
	}
	public void setMensajeNoDisponible(String mensajeNoDisponible) {
		this.mensajeNoDisponible = mensajeNoDisponible;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
		
		
}

