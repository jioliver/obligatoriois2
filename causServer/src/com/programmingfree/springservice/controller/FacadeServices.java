package com.programmingfree.springservice.controller;

import java.util.ArrayList;

import utility.Codigos;

import com.programmingfree.springservice.domain.Funcionario;
import com.programmingfree.springservice.domain.Oficina;
import com.programmingfree.springservice.domain.User;

public class FacadeServices {

	private static FacadeServices instance;
	private static ArrayList listHistorialAlumnos = new  ArrayList();
	private static ArrayList<Funcionario> listTotalFuncionarios = new  ArrayList();
	private static ArrayList<User> listTotalEstudiantes = new  ArrayList();
	private static ArrayList<Oficina> listOficinas= new ArrayList<Oficina>();
	
	public static FacadeServices getInstance(){
		if(instance==null){
			cargarDatosIniciales();
			instance = new  FacadeServices();
		}
			return instance;
	}
	
	private static void cargarDatosIniciales(){
		cargarHistorialAlumnos();
		cargarEstudiantes();
		cargarOficinas();
		cargarTotalFuncionarios();
	}
	
	private static void cargarHistorialAlumnos(){
		User user= new User();
		user.setFirstName("Juan");
		user.setUserid(132);
		user.setLastName("perez");
		user.setEmail("juan@gmail.com");
		user.setPassword("123");
		listHistorialAlumnos.add(user);
		
		User user5= new User();
		user5.setFirstName("Daniela");
		user5.setUserid(324);
		user5.setLastName("Peroni");
		user5.setEmail("daniela@gmail.com");
		listHistorialAlumnos.add(user5);
	}

	
	
	public static ArrayList cargarTotalFuncionarios(){
		
		Funcionario funcionario= new Funcionario();
		funcionario.setFirstName("Nora");
		funcionario.setUserid(01);
		funcionario.setLastName("Szazs");
		funcionario.setEmail("nora@ort.edu.uy");
		funcionario.setOficina(listOficinas.get(0));
		funcionario.setDisponible(true);
		funcionario.setPassword("123");
		funcionario.setTipo(Codigos.USUARIO_FUNCIONARIO);
		funcionario.setSexo("F");
		listTotalFuncionarios.add((Funcionario) funcionario);
		
		Funcionario funcionario2= new Funcionario();
		funcionario2.setFirstName("Santiago");
		funcionario2.setUserid(02);
		funcionario2.setLastName("Matalonga");
		funcionario2.setEmail("santiago@ort.edu.uy");
		funcionario2.setOficina(listOficinas.get(1));
		funcionario2.setDisponible(true);
		funcionario2.setPassword("123");
		funcionario2.setTipo(Codigos.USUARIO_FUNCIONARIO);
		funcionario2.setSexo("M");
		listTotalFuncionarios.add(funcionario2);
		
		Funcionario funcionario3= new Funcionario();
		funcionario3.setFirstName("Gaston");
		funcionario3.setUserid(03);
		funcionario3.setLastName("Mousques");
		funcionario3.setEmail("gaston@ort.edu.uy");
		funcionario3.setOficina(listOficinas.get(1));
		funcionario3.setDisponible(true);
		funcionario3.setPassword("123");
		funcionario3.setTipo(Codigos.USUARIO_FUNCIONARIO);
		funcionario3.setSexo("M");
		listTotalFuncionarios.add(funcionario3);
		
		Funcionario funcionario4= new Funcionario();
		funcionario4.setFirstName("Gerardo");
		funcionario4.setUserid(04);
		funcionario4.setLastName("Maturro");
		funcionario4.setEmail("gerardo@ort.edu.uy");
		funcionario4.setOficina(listOficinas.get(1));
		funcionario4.setDisponible(true);
		funcionario4.setPassword("123");
		funcionario4.setTipo(Codigos.USUARIO_FUNCIONARIO);
		funcionario4.setSexo("M");
		listTotalFuncionarios.add(funcionario4);
		
		Funcionario funcionario5= new Funcionario();
		funcionario5.setFirstName("funcionario 5");
		funcionario5.setUserid(05);
		funcionario5.setLastName("apellido");
		funcionario5.setEmail("gerardo@ort.edu.uy");
		funcionario5.setOficina(listOficinas.get(1));
		funcionario5.setDisponible(true);
		funcionario5.setPassword("123");
		funcionario5.setTipo(Codigos.USUARIO_FUNCIONARIO);
		funcionario5.setSexo("M");
		listTotalFuncionarios.add(funcionario5);
		
		Funcionario funcionario6= new Funcionario();
		funcionario6.setFirstName("Funcionario 6");
		funcionario6.setUserid(06);
		funcionario6.setLastName("Appellido");
		funcionario6.setEmail("funcionario6@ort.edu.uy");
		funcionario6.setOficina(listOficinas.get(0));
		funcionario6.setDisponible(true);
		funcionario6.setPassword("123");
		funcionario6.setTipo(Codigos.USUARIO_FUNCIONARIO);
		funcionario6.setSexo("M");
		listTotalFuncionarios.add(funcionario6);
		
		
		
		return listTotalFuncionarios;
	}
	
	
	public static void cargarEstudiantes(){
		User est= new Funcionario();
		est.setFirstName("Juan");
		est.setUserid(01);
		est.setLastName("Perez");
		est.setEmail("juan@gmail.com");
		est.setPassword("123");
		est.setTipo(Codigos.USUARIO_ESTUDIANTE);
		est.setSexo("M");
		listTotalEstudiantes.add(est);
		
		User est2= new Funcionario();
		est2.setFirstName("Pedro");
		est2.setUserid(02);
		est2.setLastName("Ruiz");
		est2.setEmail("pedro@gmail.com");
		est2.setPassword("123");
		est2.setTipo(Codigos.USUARIO_ESTUDIANTE);
		est2.setSexo("M");
		listTotalEstudiantes.add(est2);
		
		User est3= new Funcionario();
		est3.setFirstName("Daniel");
		est3.setUserid(03);
		est3.setLastName("Fernandez");
		est3.setEmail("daniel@gmail.com");
		est3.setTipo(Codigos.USUARIO_ESTUDIANTE);
		est3.setSexo("M");
		est3.setPassword("123");
		listTotalEstudiantes.add(est3);
		
		User est4= new Funcionario();
		est4.setFirstName("Daniela");
		est4.setUserid(04);
		est4.setLastName("Peroni");
		est4.setEmail("daniela@gmail.com");
		est4.setTipo(Codigos.USUARIO_ESTUDIANTE);
		est4.setSexo("F");
		est4.setPassword("123");
		listTotalEstudiantes.add(est4);
	}
	
	public static void cargarOficinas(){

		Oficina  off = new Oficina();
		off.setId(1);
		off.setNombre("Oficina 419: Coordinacion academica Ing Sistemas.");
		off.setDescripcion("Se coordinan tareas relativas a Ingenieria.");
		listOficinas.add(off);
		
		Oficina  off2 = new Oficina();
		off2.setId(2);
		off2.setNombre("Oficina 412_6: Software Factory.");
		off2.setDescripcion("Incubadora de proyectos de grado");
		listOficinas.add(off2);
		
	}
	
	public static ArrayList obtenerFuncionariosSalon(String idOficina){
		
		ArrayList<Funcionario> resp = new  ArrayList<Funcionario>();
		
		for (Funcionario funcionario: listTotalFuncionarios) {
				if(funcionario.getOficina().getId()==Integer.parseInt(idOficina)){
					resp.add(funcionario);
				}
		}
		
		return resp;
	}
	
	public static String identificarTipoUsuario(String user){
		String[] userParsed = user.split("@");
		 if(userParsed[1].equals(Codigos.DOMINIO_FUNCIONARIO)){
			 return Codigos.USUARIO_FUNCIONARIO;
		 }else{
			 return Codigos.USUARIO_ESTUDIANTE;
		 }
	}
	
	public static ArrayList getListHistorialAlumnos() {
		return listHistorialAlumnos;
	}


	public void setListHistorialAlumnos(ArrayList listHistorialAlumnos) {
		FacadeServices.listHistorialAlumnos = listHistorialAlumnos;
	}


	public ArrayList<Funcionario> getListTotalFuncionarios() {
		return listTotalFuncionarios;
	}


	public  void setListTotalFuncionarios(ArrayList listTotalFuncionarios) {
		FacadeServices.listTotalFuncionarios = listTotalFuncionarios;
	}

	@Override
	public String toString() {
		return "FacadeServices []";
	}

	public static ArrayList<User> getListTotalEstudiantes() {
		return listTotalEstudiantes;
	}

	public static void setListTotalEstudiantes(ArrayList<User> listTotalEstudiantes) {
		FacadeServices.listTotalEstudiantes = listTotalEstudiantes;
	}

	public static ArrayList<Oficina> getListOficinas() {
		return listOficinas;
	}

	public static void setListOficinas(ArrayList<Oficina> listOficinas) {
		FacadeServices.listOficinas = listOficinas;
	}
	
	
}
